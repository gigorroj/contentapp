#!/usr/bin/env python3

import webapp

class contentApp(webapp.webApp):

    content = {'/': 'Root Page', '/javier': 'Javier Page', '/J': 'J Page'}

    def parse (self, request):
        return request.split(' ',2)[1]

    def process (self, parsedRequest):
        """Process the relevant elements of the request.

        Returns the HTTP code for the reply, and an HTML page.
        """

        if parsedRequest in self.content:
            httpcode = "200 Ok"
            htmlbody = "<html><body><h1>" + self.content[parsedRequest] + "</h1></body></html>"
        else:
            httpcode = "400 Not Found"
            htmlbody = "<html><body><h1>NOT FOUND!</h1></body></html>"

        return (httpcode,htmlbody)



if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)
